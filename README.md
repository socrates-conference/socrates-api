# socrates-api

This contains the API configuration used for the SoCraTes backend. There is a default health check function written in TypeScript. It is built using yarn and webpack, and the API is managed by the AWS Serverless Application Model.

- Test: `yarn test`
- Build: `yarn build`
- Deploy: `yarn deploy`
- Clean SAM build folder: `yarn clean`

A CI build is triggered for every change you commit. A deployment can be triggered manually from the Gitlab console.