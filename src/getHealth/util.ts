import {APIGatewayProxyResult} from 'aws-lambda'

export const cors = (response: APIGatewayProxyResult) => {
  response.headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': 'GET, PUT, OPTIONS, DELETE'
  }
  return response
}
export const success = (payload?: any): APIGatewayProxyResult => cors({
  statusCode: 200,
  body: payload !== undefined ? JSON.stringify(payload) : undefined
})
export const failure = (code: number = 500, reason: string = 'Internal server error'): APIGatewayProxyResult => cors({
  statusCode: code,
  body: JSON.stringify({reason: reason})
})